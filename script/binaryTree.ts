interface nodeType<T> {
    data: T | null;
    left: nodeType<T> | null;
    right: nodeType<T> | null;
}

class BinaryTree<T> {
    constructor(public left: nodeType<T> | null = null, 
        public right: nodeType<T> | null = null,
        public data: T | null = null) {};

    insert(data: T, node: nodeType<T> | undefined | null): undefined | null{
        node = node || this;
        if (node.data === null) {
            node.data = data;
            return null;
        }
        if (node.data > data) {
            if (node.left === null) {
                node.left = new BinaryTree();
            }

            return this.insert(data, node.left);
        } else if (node.data < data) {
            if (node.right === null) {
                node.right = new BinaryTree();
            }

            return this.insert(data, node.right);
        }
    }

    search(data: T, node: nodeType<T> | null): null | nodeType<T>{
        node = node || this;

        if (node.data === data) {
            return node;
        } else if (node.data === null) {
            return null;
        }

        if (node.data > data && !(node.left === null && node.right === null)) {
            return this.search(data, node.left);
        } else if (node.data < data && !(node.left === null && node.right === null)) {
            return this.search(data, node.right);
        }
        
        return null;
    }

    remove(data: T, node: nodeType<T> | null | undefined) {
        node = node || this;

        const leftNode = function (node: nodeType<T>): undefined | nodeType<T> {
            if (node.left === null) {
                return node;
            } else if (node.left !== null) {
                return leftNode(node.left);
            }
            return;
        }

        if(node.left === null && node.right === null){
            return null;
        } else if (node === null) {
            return null;
        } else if (data > node.data!) {
            node.right = this.remove(data, node.right);
            return node;
        } else if (data < node.data!) {
            node.left = this.remove(data, node.left);
            return node;
        } else {
            if (node.left === null) {
                node = node.right;
                return node;
            } else if (node.right === null) {
                node = node.left;
                return node;
            }
            let newNode: nodeType<T> | undefined | null = leftNode(node.left);
            node.data = newNode!.data;
            node.right = this.remove(newNode!.data!, node.right);

            return node;
        }
    }
}
