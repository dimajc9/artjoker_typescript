///// 1 /////
const anagram = (firstlLine: string, secondLine: string): boolean => {
    let result: string = '';

    if (firstlLine.length == secondLine.length) {
        for (let i: number = 0; i < firstlLine.length; i++) {
            for (let j: number = 0; j < secondLine.length; j++) {
                if (firstlLine[i] == secondLine[j]) {
                    result += firstlLine[i];
                    break;
                }
            }
        }
    }

    if (firstlLine == result) {
        return true;
    }

    return false;
}

///// 3 /////
const numberCounter = (nums: number): Record<string, number> => {
    let mass_nums: string[] = nums.toString().split('');
    let obj: Record<string, number> = {};
    for (let i in mass_nums) {
        obj[mass_nums[i]] = ++obj[mass_nums[i]] || 1;
    }

    return obj;
}

///// 4 /////
const numberUniqueWords = (str: string): number => {
    let strings: string[] = str.replace(/\-/g, '').split(' ');
    let obj: Record<string, number> = {};

    for (let i in strings) {
        obj[strings[i]] = ++obj[strings[i]] || 1;
    }

    let arrayValues: number[] = Object.values(obj);
    let numberUniqueWords: number = 0;

    for (let i: number = 0; i < arrayValues.length; i++) {
        if (arrayValues[i] == 1) {
            numberUniqueWords += 1;
        }
    }

    return numberUniqueWords;
}

///// 5 /////
const occurrenceEachWord = (str: string): Record<string, number> => {
    let strings: string[] = str.split(' ');
    let obj: Record<string, number> = {};

    for (let i in strings) {
        obj[strings[i]] = ++obj[strings[i]] || 1;
    }

    return obj;
}

///// 6 /////
const fibonacci = (index: number) => {
    let numb: Array<number> = [0, 1];

    if(index !== 0 && index !== 1){
        index = index / 2;
        for (let i: number = 0; i < index * 2 - 2; i++) {
            numb.push(numb[i] + numb[i+1]);
        }
        return numb;
    } 

    return index
}

///// 7 /////
const perimeter = {
    rectangle(sideOne: number, sideTwo: number): number {
        return (sideOne + sideTwo) * 2;
    },
    triangle(sideOne: number, sideTwo: number, sideThree: number): number {
        return sideOne + sideTwo + sideThree;
    },
    circle(sideOne: number): number {
        return 2 * Math.PI * sideOne;
    },
};

const square = {
    rectangle (sideOne: number, sideTwo: number): number {
        return sideOne * sideTwo;
    },
    triangle(sideOne: number, sideThree: number): number {
        return (sideOne * sideThree) / 2;
    },
    circle(sideOne: number): number {
        return ( 2 * Math.PI * sideOne * (sideOne * 2) ) / 4;
    }
};

///// 8 /////
const factorial = (num: number): number => {
    let result: number = 1;

    for (let i: number = 0; i < num; i++) {
       result += num - 1
    }

    return result;
}

///// 9 /////
const multiplesCallback = (num: number, callback: Function): number => {
    let mass: number[] = num.toString().split('').map(Number);
    let result: number = 0;

    for (let i: number = 0; i < mass.length; i++) {
        if (callback(mass[i])) {
            result += mass[i];
        }
    }

    return result;
}

const sumPositiveNumbers = (num: Array<number>): number => {
    let result: number = 0;

    for (let i: number = 0; i < num.length; i++) {
        if (num[i] > 0) {
            result += num[i];
        }
    }

    return result;
}

///// 10 /////
const numberArrayElements = (mass: number[], callback: Function): number => {
    let result: number = 0;

    for (let i: number = 0; i < mass.length; i++) {
        if (callback(mass[i])) {
            result++;
        }
    }

    return result;
}
    
///// 11 /////
const shiftingToBinary = (num: number): string => {
    let result: string = '';
    let bit: number = 1;

    while (num >= bit) {
        result = (num & bit ? 1 : 0) + result;
        bit <<= 1;
    }

    return result;
}

const shiftingToDecimal = (num: string): number => {
    let result: number = 0;
    let len: number = num.length;
    let bit: number = 1;

    while (len--) {
        result += num[len] === '1' ? bit : 0;
        bit <<= 1;
    }

    return result;
}

///// 12 ////
const parsingMultidimensionalArray = (mass: number[][], callback: Function): number => {
    let result: number = 0;

    for (let i: number = 0; i < mass.length; i++) {
        for (let j: number = 0; j < mass[i].length; j++) {
            if (callback(mass[i][j])) {
                result += mass[i][j];
            }
        }
    }

    return result;
}

///// 13 /////
const sumValuesNumbers = (min: number, max: number, callback: Function): number => {
    let result: number = 0;

    for (let index: number = min; index <= max; index++) {
        if (callback(index)) {
            result += index;
        }
    }

    return result;
}

///// 14 /////
const averageValueArray = (mass: number[]): Record<string, number> => {
    let massFlat: number[] = mass.flat();
    let resultMean: Record<string, number> = {
        even: 0,
        notEven: 0,
    }

    for (let i: number = 0; i < massFlat.length; i++) {
        if (massFlat[i] % 2 == 0) {
            resultMean.even += massFlat[i] / 2;
        } else {
            resultMean.notEven += massFlat[i] / 2;
        }
    }

    return resultMean;
}

///// 15 /////
const matrix: number[][] = [
    [1, 2, 4, 45],
    [3, 4, 7, -2],
    [5, 6, 13, 54]
];

const transposedMatrix = (matrix: number[][]) => {
    return matrix[0].map((item, index) => {
        return matrix.map(row => row[index])
    });
}

///// 16 /////
let matricesOne: number[][] = [
    [1, 3, 7],    
    [3, 8, 2],
    [2, 9, 0]
];
let matricesTwo: number[][] = [
    [1, 2, 2],
    [6, 2, 7],
    [1, 7, 9]
];

const foldMatrices = (matricesOne: number[][], matricesTwo: number[][]) => {
    let result: number[][] = [        
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ];

    for (let i: number = 0; i < matricesOne.length; i++) {
        for (let j: number = 0; j < matricesOne[i].length; j++) {
            result[i][j] += matricesOne[i][j] + matricesTwo[i][j];
        }
    }

    return result;
}

///// 17 /////
const removeFromArrayString = (mass: number[][]): number[][] => {
    for (let i: number = 0; i < mass.length; i++) {
        for (let j: number = 0; j < mass[i].length; j++) {
            if (mass[i][j] == 0) {
                mass.splice(i, 1);
                break;
            }
        }
    }

    return mass;
}

const removeFromArrayСolumn = (mass: number[][]): number[][] => {
    const deleteСolumn = (index: number): void => {
        for (let i: number = 0; i < mass.length; i++) {
            mass[i].splice(index, 1);
        }
    }

    for (let i: number = 0; i < mass.length; i++) {
        for (let j: number = 0; j < mass[i].length; j++) {
            if (mass[i][j] == 0) {
                deleteСolumn(j);
                break;
            }
        }    
    }

    return mass;
}