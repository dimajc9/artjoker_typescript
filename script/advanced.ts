Object.defineProperty(Array.prototype, "myReduce", {
    value: function (fun: Function, accumulytor: any): any {
        accumulytor = accumulytor || this[0];

        for (let i: number = 0; i < this.length; i++) {
            accumulytor = fun(accumulytor, this[i], i, this);
        }

        return accumulytor;
    }
});

Object.defineProperty(Array.prototype, "myFilter", {
    value: function (compare: Function): any {
        let result: any = [];

        for (let i: number = 0; i < this.length; i++) {
            if (compare(this[i])) {
                result.push(this[i]);
            }
        }

        return result;
    }
});

Object.defineProperty(Array.prototype, "myForEach", {
    value: function (compare: Function, index: number) {
        index = index || 0;

        if (index < this.length) {
            compare(this[index], this);
            this.myForEach(compare, ++index);
        }

    }
});

Object.defineProperty(Array.prototype, "myMap", {
    value: function (compare: Function): any {
        let result: any = [];

        for (let i: number = 0; i < this.length; i++) {
            result.push(compare(this[i]));
        }

        return result;
    }
});

Object.defineProperty(Function.prototype, "myBind", {
    value: function (context: object, ...args: Array<number | string>) {
        let object: any = {
            ...context,
        };
    
        let symbol: symbol = Symbol();
        object[symbol] = this;
    
        return function (...rest: Array<number | string>) {
            let result = object[symbol](...args, ...rest);
            delete object[symbol];
            return result;
        };
    }
});

Object.defineProperty(Function.prototype, "myCall", {
    value: function (object: Record<number | string | symbol, Function>, ...args: any) {
        let symbol: symbol = Symbol();
        object[symbol] = this;
        let result: any = object[symbol](...args);
        delete object[symbol];
        return result;
    }
});

let fibonacciNumberIterator = {
    min: 0,
    max: 10,
    [Symbol.iterator]() {
        let current: number = this.min;
        let max: number = this.max;
        let numb: number[] = [0, 1];
        let result: number | undefined = 0;
        return {
            next() {
                if (current >= max) {
                    result = undefined;
                } else {
                    numb.push(numb[current] + numb[current + 1]);
                    result = numb[current];
                }
                current++;

                return {
                    value: result,
                    done: current > max,
                };
            }
        };
    },
};

function* fibonachi(n: number, current: number, next: number): Generator<number> {
    current = current || 0;
    next = next || 1;

    if (n == 0) {
        return current;
    }

    yield current;
    yield* fibonachi(n - 1, next, current + next);
};
