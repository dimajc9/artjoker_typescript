///// 3 /////
const numberCounterRecusrion = (nums: number, index: number, result: Record<string, number>): Record<string, number> => {
    index = index || 0;
    result = result || {};
    let mass_nums: string[] = nums.toString().split('');
    if (mass_nums.length > index) {
        result[mass_nums[index]] = ++result[mass_nums[index]] || 1;
        return numberCounterRecusrion(nums, ++index, result);
    }

    return result;
}

// ///// 4 /////
const numberOfUniqueWordsRecusrion = (str: string, index: number, obj: Record<string, number>): number => {
    index = index || 0;
    obj = obj || {};
    let strings: string[] = str.replace(/\-/g, '').split(' ');

    if (strings.length > index) {
        obj[strings[index]] = ++obj[strings[index]] || 1;
        return numberOfUniqueWordsRecusrion(str, ++index, obj);
    }
    let arrayOfValues: number[] = Object.values(obj);
    let counter: number = 0;

    for (let i: number = 0; i < arrayOfValues.length; i++) {
        if (arrayOfValues[i] === 1) {
            counter += 1;
        }
    }

    return counter;
}

///// 5 /////
const numberOccurrenceEachWordRecusrion = (str: string, index: number, result: Record<string, number>): Record<string, number> => {
    index = index || 0;
    result = result || {};

    let strings: string[] = str.split(' ');
    if (strings.length > index) {
        result[strings[index]] = ++result[strings[index]] || 1;
        numberOccurrenceEachWordRecusrion(str, ++index, result);
    }

    return result;
}

///// 6 /////
const fibonacciRecusrion = (numberCycles: number, index: number, numbFibonacc: number[]): number[] => {
    index = index || 0;
    numberCycles = numberCycles || 0;
    numbFibonacc = numbFibonacc || [0, 1];

    if (numberCycles / 2 > index) {
        numbFibonacc[0] = numbFibonacc[0] + numbFibonacc[1], numbFibonacc[1] = numbFibonacc[0] + numbFibonacc[1];
        fibonacciRecusrion(numberCycles, ++index, numbFibonacc);
    }

    return numbFibonacc;
}

///// 8 /////
const factorialRecusrion = (num: number): number => {
    return (num != 1) ? num * factorialRecusrion(num - 1) : 1;
}

///// 9 /////
const multiplesCallbackRecusrion = (numb: number, index: number, result: number, callback: Function): number => {
    index = index || 0;
    result = result || 0;
    let mass: number[] = numb.toString().split('').map(Number);
    if (mass.length > index) {
        if (callback(mass[index])) {
            result += mass[index];
            return multiplesCallbackRecusrion(numb, ++index, result, callback);
        } else {
            result = multiplesCallbackRecusrion(numb, ++index, result, callback);
        }
    }

    return result;
}

///// 10 /////
const sumPositiveNumbersRecursion = (mass: number[], result: number, index: number): number => {
    result = result || 0;
    index = index || 0;

    if (mass.length > index) {
        if (mass[index] == 0) {
            result++;
            return sumPositiveNumbersRecursion(mass, result, ++index);
        } else {
            result = sumPositiveNumbersRecursion(mass, result, ++index);
        }
    }

    return result;
}

///// 12 ////
const parsingMultidimensionalArrayRecursion = (mass: number[][], callback: Function) => {
    let pavedMass: number[] = [];
    let result: number = 0;

    const parseArray = (mass: number[] | number[][]): void => {
        for (let item of mass) {
            if (Array.isArray(item)) {
                parseArray(item);
            } else {
                pavedMass.push(item);
            }
        }
    }

    parseArray(mass);

    for (let index in pavedMass) {
        if (callback(pavedMass[index])) {
            result += pavedMass[index];
        }
    }

    return result;
}

///// 13 /////
const sumValuesNumbersRecusrion = (min: number, max: number, result: number, callback: Function): number => {
    result = result || 0;
    if (++min <= max) {
        if (callback) {
            result += min;
        }
        result = sumValuesNumbersRecusrion(min, max, result, callback);
    }

    return result;
}

///// 14 /////
const averageValueArrayRecusrion = (mass: number[]) => {
    let massFlat: number[] = mass.flat();
    let resultMean : Record<string, number> = {
        even: 0,
        notEven: 0,
    }

    const parsingArray = (massFlat: number[], counter: number): void => {
        counter = counter || 0;
        if (massFlat.length > counter) {
            if (massFlat[counter] % 2 == 0) {
                resultMean.even += massFlat[counter] / 2;
            } else {
                resultMean.notEven += massFlat[counter] / 2;
            }

            parsingArray(massFlat, ++counter);
        }
    }
    parsingArray(massFlat, 0);

    return resultMean;
}

///// 16 /////
let matricesOneRecursion: number[][] = [
    [1, 3, 7],
    [3, 8, 2],
    [2, 9, 0]
];

let matricesTwoRecursion: number[][] = [
    [1, 2, 2],
    [6, 2, 7],
    [1, 7, 9]
];

const addsTwoMatricesRecusrion = (matricesOneRecursion: number[][], matricesTwoRecursion: number[][], count: number, result:number[][]): number[][] => {
    count = count || 0;
    result = result || [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ];

    if (count < matricesOneRecursion.length) {
        for (let index in matricesOneRecursion[count]) {
            result[count][index] += matricesOneRecursion[count][index] + matricesTwoRecursion[count][index];
        }

        addsTwoMatricesRecusrion(matricesOneRecursion, matricesTwoRecursion, ++count, result);
    }

    return result;
}

///// 17 /////
const removeFromArrayStringRecusrion = (mass: number[][], index: number) => {
    index = index || 0;

    if (index++ < mass.length) {
        for (let i in mass[index]) {
            if (mass[index][i] == 0) {
                mass.splice(index, 1);
                --index;
                break;
            }
        }

        removeFromArrayStringRecusrion(mass, index);
    }

    return mass;
}

const removeFromArrayСolumnRecusrion = (mass: number[][], index: number): number[][] => {
    index = index || 0;
    const deleteСolumn = (data: number) => {
        for (let index in mass) {
            mass[index].splice(data, 1);
        }
    }

    if (index++ < mass.length) {
        for (let i of mass[index]) {
            if (mass[index][i] === 0) {
                deleteСolumn(i);
                break;
            }
        }

        removeFromArrayСolumnRecusrion(mass, index);
    }

    return mass;
}
